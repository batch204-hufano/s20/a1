// console.log("Hello World");

let providedNumber = Number(prompt("Enter a number:"));
console.log("The number you provided is " + providedNumber + ".");

for (providedNumber; providedNumber >= 0; providedNumber--) {

	if (providedNumber <= 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	if ((providedNumber % 10) === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if ((providedNumber % 5) === 0) {
		console.log(providedNumber);
	}
}

/********************************/

let string = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < string.length; i++) {

	if (string[i] === "a" ||
		string[i] === "e" ||
		string[i] === "i" ||
		string[i] === "o" ||
		string[i] === "u" 
	) {
		
		continue;

	} else {
		consonants += string[i];
	}
}

console.log(string);
console.log(consonants);
